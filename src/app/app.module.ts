import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AuthInterceptor } from './auth.interceptor';
import { TvmazeModule } from './features/tvmaze/tvmaze.module';
import { SharedModule } from './shared/shared.module';
import { SettingsModule } from './features/settings/settings.module';
import { PexelsModule } from './features/pexels/pexels.module';
import { CoreModule } from './core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { LoginModule } from './features/login/login.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    // Custom
    CoreModule,
    SharedModule,
    AppRoutingModule,
    // features
    LoginModule,
    PexelsModule,
    TvmazeModule,
    SettingsModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
