import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'fb-pexels',
  template: `
    <form #f="ngForm" (ngSubmit)="submit()">
      <input [placeholder]="'Search ' + (type === 'image' ? 'Image' : 'Video')" type="text" ngModel name="text" required>
      <select [ngModel]="type" name="type" (ngModelChange)="reset()">
        <option value="image">Image</option>
        <option value="video">Video</option>
      </select>
      <button type="submit" [disabled]="f.invalid" hidden>GO</button>
    </form>
    
    <div style="width: 100%; overflow: auto;">
      <div style="display: flex">
        
        <!--images-->
        <ng-container *ngIf="type === 'image'">
          <div
            style="flex-grow: 0"
            *ngFor="let img of images"
          >
            <img [src]="img.src.small" [alt]="img.photographer" (click)="selectedItem = img">
          </div>
        </ng-container>

        <!--videos-->
        <ng-container *ngIf="type === 'video'">
          <div 
            style="flex-grow: 0"
            *ngFor="let video of videos"
          >
            <img [src]="video.image" (click)="selectedItem = video" height="100">
          </div>
        </ng-container>
        
      </div>
    </div>
    
    <!--preview image-->
    <img 
      *ngIf="selectedItem && type === 'image'" 
      [src]="selectedItem?.src?.medium" width="100%"
    >

    <!--preview video-->
    <video 
      [src]="selectedItem.video_files[0].link"
      width="100%"
      *ngIf="selectedItem && type ==='video'" controls></video>
    
    <pre>{{selectedItem | json}}</pre>

  `,
  styles: [
  ]
})
export class PexelsComponent {
  @ViewChild('f') form: NgForm;
  type = 'video'
  images: any[];
  videos: any[];
  selectedItem: any;

  constructor(private http: HttpClient) {}

  submit() {
    if (this.form.value.type === 'image') {
      this.searchImage(this.form.value.text);
    } else {
      this.searchVideo(this.form.value.text);
    }
  }

  searchImage(text: string) {
    this.http.get<any>( 'https://api.pexels.com/v1/search?per_page=10&query=' + text)
      .subscribe(
        res => {
          this.images = res.photos;
        });
  }

  searchVideo(text: string) {
    this.http.get<any>( 'https://api.pexels.com/videos/search?per_page=10&query=' + text)
      .subscribe(
        res => {
          this.videos = res.videos;
        });
  }

  reset() {
    this.type = this.form.value.type;
    this.images = [];
    this.videos = [];
    this.selectedItem = null;
    if (this.form.valid) {
      this.submit();
    }
  }
}
