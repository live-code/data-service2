import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PexelsComponent } from './pexels.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    PexelsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class PexelsModule { }
