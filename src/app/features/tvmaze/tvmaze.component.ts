import { Component } from '@angular/core';
import { TvmazeService } from './services/tvmaze.service';

@Component({
  selector: 'fb-tvmaze',
  template: `
    <fb-hero title="TV MAZZE" image="https://images.pexels.com/photos/1117132/pexels-photo-1117132.jpeg?auto=compress&cs=tinysrgb&h=350"></fb-hero>
    
    <fb-tvmaze-search 
      (search)="tvMazeService.submit($event)"></fb-tvmaze-search>
    
    <fb-tvmaze-result 
      [items]="tvMazeService.result" 
      (itemClick)="tvMazeService.itemClickHandler($event)"></fb-tvmaze-result>
    
    <fb-tvmaze-modal 
      [show]="tvMazeService.selectedItem" 
      (close)="tvMazeService.closeModal()"></fb-tvmaze-modal>
  `,
})
export class TvmazeComponent {
  constructor(public tvMazeService: TvmazeService) {
  }
}
