import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Series } from '../model/series';

@Component({
  selector: 'fb-tvmaze-result',
  template: `
    <div class="grid">
      <div
        class="grid-item"
        *ngFor="let series of items"
      >
        <div class="movie" (click)="itemClick.emit(series)">
          <img *ngIf="series.show.image" [src]="series.show.image?.medium" width="100%">
          <div *ngIf="!series.show.image" class="no-image"></div>
          <div class="movie-text">{{series.show.name}}</div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./tvmaze-result.component.css']
})
export class TvmazeResultComponent {
  @Input() items: Series[];
  @Output() itemClick: EventEmitter<Series> = new EventEmitter()

  render() {
    console.log('list: render')
  }
}
