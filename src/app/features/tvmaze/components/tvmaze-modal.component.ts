import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Show } from '../model/series';

@Component({
  selector: 'fb-tvmaze-modal',
  template: `
    <div *ngIf="show" class="wrapper">
      <div class="content">
        
        <span
          class="closeButton"
          (click)="close.emit()"
        >×</span>

        <img [src]="show.image.original" class="image">

        <div class="metadata">
          <h1>{{show.name}}</h1>
          <div class="tag" *ngFor="let g of show.genres">{{g}}</div>

          <div [innerHTML]="show.summary"></div>

          <a
            class="button"
            target="_blank"
            rel="noopener noreferrer"
            [href]="show.url">Visit WebSite</a>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./tvmaze-modal.component.css']
})
export class TvmazeModalComponent implements OnInit {
  @Input() show: Show;
  @Output() close: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
  }

}
