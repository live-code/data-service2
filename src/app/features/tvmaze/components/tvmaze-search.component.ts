import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'fb-tvmaze-search',
  template: `
    <form #f="ngForm" (submit)="search.emit(f.value)">
      <input
        (focus)="showText()"
        (blur)="description = null"
        type="text" [ngModel] name="text" required placeholder="Search TV Series">
      <small>{{description}}</small>
      <button type="submit" [disabled]="f.invalid" hidden></button>
    </form>
  `,
  styles: [
  ]
})
export class TvmazeSearchComponent {
  @Output() search: EventEmitter<{ text: string}> = new EventEmitter()
  description: string;

  showText() {
    this.description = 'Write the name of your favorite tv series...'
    console.log('show text')
  }

  render() {
    console.log('form render')
  }
}
