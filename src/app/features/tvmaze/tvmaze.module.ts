import { NgModule } from '@angular/core';
import { TvmazeComponent } from './tvmaze.component';
import { TvmazeSearchComponent } from './components/tvmaze-search.component';
import { TvmazeResultComponent } from './components/tvmaze-result.component';
import { TvmazeModalComponent } from './components/tvmaze-modal.component';
import { FormsModule } from '@angular/forms';
import { HeroComponent } from '../../shared/components/hero.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    // HeroComponent,
    TvmazeComponent,
    TvmazeSearchComponent,
    TvmazeResultComponent,
    TvmazeModalComponent,
  ],
  imports: [
    FormsModule,
    SharedModule
  ],
  exports: [
  ]
})
export class TvmazeModule {

}
