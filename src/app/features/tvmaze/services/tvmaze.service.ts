import { Injectable } from '@angular/core';
import { Series, Show } from '../model/series';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TvmazeService {
  private _result: Series[];
  selectedItem: Show;

  constructor(private http: HttpClient) { }

  submit(formData: { text: string}) {
    this.http.get<Series[]>('http://api.tvmaze.com/search/shows?q=' + formData.text)
      .subscribe( res => this._result = res);
  }

  itemClickHandler(series: Series) {
    this.selectedItem = series.show;
  }

  closeModal() {
    this.selectedItem = null;
  }

  get result() {
    return this._result;
  }
}
