import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/settings/theme.service';

@Component({
  selector: 'fb-settings',
  template: `
    <fb-hero title="SETTINGS" image="https://images.pexels.com/photos/270700/pexels-photo-270700.jpeg?auto=compress&cs=tinysrgb&h=350"></fb-hero>
    
    <button
      [ngClass]="{'active': themeService.value === 'dark'}"
      (click)="themeService.value = 'dark'">
      Dark
    </button>

    <button
      [ngClass]="{'active': themeService.value === 'light'}"
      (click)="themeService.value = 'light'">
      light
    </button>
  `,
  styles: [`
    .active {
      background-color: orange;
    }
  `]
})
export class SettingsComponent  {

  constructor(public themeService: ThemeService) {
    console.log(themeService.value);
  }


}
