import { NgModule } from '@angular/core';
import { HeroComponent } from './components/hero.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    HeroComponent,
  ],
  imports: [
    BrowserModule,
  ],
  exports: [
    HeroComponent,
  ]
})
export class SharedModule { }
