import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'fb-hero',
  template: `
    <div style="width: 100%; height: 200px; overflow: hidden; position: relative">
      <h1 *ngIf="title" class="title">{{title}}</h1>
      <img 
        width="100%"
        [ngClass]="{'pippo': true}"
        [src]="image" alt="">
    </div>
    
  `,
  styles: [`
    .title {
      position: absolute;
      top: 20px;
      left: 20px
    }
  `]
})
export class HeroComponent {
  @Input() title = 'DEFAULT TITLE'
  @Input() image =  'https://images.pexels.com/photos/459225/pexels-photo-459225.jpeg?auto=compress&cs=tinysrgb&h=600';

}
