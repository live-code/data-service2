import { Component } from '@angular/core';
import { TvmazeService } from './features/tvmaze/services/tvmaze.service';
import { ThemeService } from './core/settings/theme.service';

@Component({
  selector: 'fb-root',
  template: `
    <div
      [ngClass]="{
        'dark': themeService.value === 'dark',
        'light': themeService.value === 'light'
      }"
    >
      <fb-navbar></fb-navbar>
      <router-outlet></router-outlet>
    </div>
    <div *ngIf="tvMazeService.result" style="text-align: center">
      <hr>
      {{tvMazeService.result?.length}} risultati
    </div>
  `,
  styles: [`
    .dark {
      background: linear-gradient(#222222 50%, rgba(255,255,255,0) 0) 0 0,
      radial-gradient(circle closest-side, #222222 53%, rgba(255,255,255,0) 0) 0 0,
      radial-gradient(circle closest-side, #222222 50%, rgba(255,255,255,0) 0) 55px 0 #48B;
      background-size: 110px 200px;
      background-repeat: repeat-x;
    }
    .light {
      background: linear-gradient(#ccc 50%, rgba(255,255,255,0) 0) 0 0,
      radial-gradient(circle closest-side, #ccc 53%, rgba(255,255,255,0) 0) 0 0,
      radial-gradient(circle closest-side, #ccc 50%, rgba(255,255,255,0) 0) 55px 0 #48B;
      background-size: 110px 200px;
      background-repeat: repeat-x;
    }
  `]
})
export class AppComponent {
  constructor(
    public tvMazeService: TvmazeService,
    public themeService: ThemeService
  ) {
  }
}
