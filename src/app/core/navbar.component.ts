import { Component, OnInit } from '@angular/core';
import { ThemeService } from './settings/theme.service';

@Component({
  selector: 'fb-navbar',
  template: `
    <div style="width: 100%; padding: 10px">
      <button
        [ngClass]="{
          'dark': themeService.value === 'dark',
          'light': themeService.value === 'light'
        }"
        [routerLink]="'tvmaze'">TVMAZE</button>
      <button
        [ngClass]="{
          'dark': themeService.value === 'dark',
          'light': themeService.value === 'light'
        }"
        routerLink="pexels">PEXELS</button>
      <button
        [ngClass]="{
          'dark': themeService.value === 'dark',
          'light': themeService.value === 'light'
        }"
        routerLink="settings">SETTINGS</button>

      <button
        [ngClass]="{
          'dark': themeService.value === 'dark',
          'light': themeService.value === 'light'
        }"
        routerLink="login">LOGIN</button>

      <button
        [ngClass]="{
          'dark': themeService.value === 'dark',
          'light': themeService.value === 'light'
        }"
        routerLink="contacts">CONTACTS</button>
    </div>
    
    <hr>

  `,
  styles: [`
    .dark {
      background-color: #222222;
      color: white;
    }
    
    .light {
      background-color: #ccc;
      color: #222;
    }
  `]
})
export class NavbarComponent implements OnInit {

  constructor(
    public themeService: ThemeService
  ) { }

  ngOnInit(): void {
  }

}
