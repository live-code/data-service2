import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

export class AuthInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.includes('api.pexels')) {
      const cloned = req.clone({
        setHeaders: {
          Authorization: '563492ad6f9170000100000189ac030285b04e35864a33b95c2838be'
        }
      });
      return next.handle(cloned);
    }
    return next.handle(req)
  }

}
