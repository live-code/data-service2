import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TvmazeComponent } from './features/tvmaze/tvmaze.component';
import { PexelsComponent } from './features/pexels/pexels.component';
import { SettingsComponent } from './features/settings/settings.component';
import { LoginComponent } from './features/login/login.component';

const routes = [
  { path: 'tvmaze', component: TvmazeComponent},
  { path: 'pexels', component: PexelsComponent},
  { path: 'settings', component: SettingsComponent},
  { path: 'login', component: LoginComponent},
  { path: '', pathMatch: 'full', redirectTo: 'tvmaze'},
  { path: 'contacts', loadChildren: () => import('./features/contacts/contacts.module').then(m => m.ContactsModule) }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
